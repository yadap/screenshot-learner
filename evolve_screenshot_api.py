"""
Evolve API for image classification.
"""
__author__ = "Justin Oliver and Yada Pruksachatkan"

from base64 import urlsafe_b64decode
from collections import defaultdict
from cStringIO import StringIO
import tensorflow_classifier as tfc
from tensorflow_classifier import classify_image as tf_classify
from svm_classifier import classify_image as svm_classify
from flask import Flask, jsonify, request
import os
from signal import signal, SIGPIPE, SIG_DFL
from text_extraction import get_text
from time import time
from werkzeug.utils import secure_filename
try:
    import Image
except ImportError:
    from PIL import Image

signal(SIGPIPE,SIG_DFL)
app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = 'uploads/'

classify_methods = {
    'tensorflow': tf_classify,
    'svm': svm_classify,
}


@app.route('/')
def index():
    return "It works!"


@app.route('/classifications/', methods=['POST'])
def classify_screenshot():
    """
    Returns the file classification and a file ID to uniquely
    identify the file if the file needs to be reclassified.
    """
    result = {'status': False}
    image = request.files['file']
    classify = request.args.get('method', 'tensorflow')
    
    if image.filename:
        # Decode and save the image
        filename = secure_filename(image.filename) + str(time())
        path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
        data = StringIO(urlsafe_b64decode(image.read()))
        Image.open(data).save(path, "JPEG")

        # Classify the image and return the results if the tag was valid
        tag = classify_methods[classify](path)
        if tag:
            name, age = get_text(tag, path)
            result = {
                'fileid': filename,
                'status': True,
                'tag': tag,
                'name': name,
                'age': age,
            }

    return jsonify(result)

if __name__ == '__main__':
    app.run(debug=True)

