"""
SVM image classifier.
TODO: Clean up and add a finetune method.
"""

from __future__ import division
from __future__ import print_function
from PIL import Image
from sklearn import svm
import os
import numpy as np
import time


__author__ = "Justin Oliver and Yada Pruksachatkan"


class Classifier(object):
    """
    Helper class for encapsulating the parameters for building a classifier.
    """
    def __init__(self, category, classifier):
        self.category = category
        self.classifier = classifier


class ImageClassifier(object):
    """
    Helper class for encapsulating a classifier with its images.
    """
    def __init__(self, classifier, image_train):
        self.classifier = classifier
        self.image_train = image_train


class OneVsAllClassifier:
    """
    OneVsAllClassifier is a classifier that uses SVMs
    to accpet or reject
    """

    def __init__(self, categories_path, classifiers):
        self.classifiers = {}

        for c in classifiers:
            path = os.path.join(categories_path, c.category)
            clfr = self._initialize_classifier(path, c.classifier)
            self.classifiers[c.category] = [clfr.classifier, clfr.image_train]

    def _initialize_classifier(self, path, classifier):
        """
        Trains a classifier for the images in the passed directory.
        """
        image_train = self._get_images(path)
        result = np.array([1]*len(image_train))
        classifier.fit(image_train, result)
        return ImageClassifier(classifier, image_train)

    def _get_images(self, path):
        """
        Returns the images found in the passed directory.
        """
        images = []
        for subdir, dirs, files in os.walk(path):
            for image_path in files:
                img = Image.open(os.path.join(subdir, image_path))
                img = img.resize((400,400), Image.ANTIALIAS)
                img = np.array(img)
                img = img[:,:,0]
                img = img.reshape(1, img.shape[0]* img.shape[1])
                images.append(img[0])
        return images

    def test_accuracy(self):
        """
        Function to test accuracy of classiifers
        """
        other_images = []
        cflist = []
        for key, value in self.classifiers.iteritems():
            temp = [key,value]
            cflist.append(temp)
        for c in cflist:
            other_images = []
            for subdir, dirs, files in os.walk("resources/categories"):
                if ((subdir != "resources/categories/"+c[0]) and
                    (subdir != "resources/categories")):
                    for sd, d, f in os.walk(subdir):
                        for file_new in f:
                            img = Image.open(os.path.join(sd, file_new))
                            img = img.resize((400,400), Image.ANTIALIAS)
                            img = np.array(img)
                            img = img[:,:,0]
                            img = img.reshape(1, img.shape[0]* img.shape[1])
                            other_images.append(img[0])
            clfr = c[1][0]
            result_new = np.array([-1]*len(other_images))
            predictions =  np.array(clfr.predict(other_images))
            error = np.mean( predictions != result_new)
            print("Percentage of false positives for " + c[0]) # positive meaning it is Tinder
            print(error)


def classify_image(image_path, category, OneVsAllClassifier):
    img = Image.open(image_path) # relative path
    img = img.resize((400,400), Image.ANTIALIAS)
    img = np.array(img)
    img = img[:,:,0]
    img = img.reshape(1, img.shape[0]* img.shape[1])
    clfr = OneVsAllClassifier.classifiers[category]
    return clfr.predict([img])[0]


if __name__ == '__main__':
    path = "resources/categories/"
    # currently, the best is rbf - with0.3103 fplse negative and 0.0 false pos for nu=0.7 rbf
    classifiers = (
        Classifier("tinder", svm.OneClassSVM(kernel="rbf", nu=0.01, gamma=0.01)),
        Classifier("bumble", svm.OneClassSVM(kernel="poly", nu=0.01, gamma=0.1)),
        Classifier("okcupid", svm.OneClassSVM(kernel="rbf", nu=0.1, gamma=0.01)),
        Classifier("coffeemeetsbagel", svm.OneClassSVM(kernel="rbf", nu=0.7)),
    )
    clfr = OneVsAllClassifier(path, classifiers)
    clfr.test_accuracy()

