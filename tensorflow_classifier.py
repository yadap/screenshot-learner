"""
For testing through python, change and run this code.
Image classifier using TensorFlow.

For preparing the model, take a look at the following links:
 - https://www.tensorflow.org/tutorials/image_retraining
 - https://github.com/eldor4do/TensorFlow-Examples/blob/master/retraining-example.py

>>> import date_classifier as dc
>>> dc.run_reference_on_image("/path/to/model.pb", "/path/to/image.jpg",
                              >>> "/path/to/labels.txt")
"""

import argparse
import numpy as np
import sys
import tensorflow as tf


sess = None
graph = None


def create_graph(model_file):
    """Creates a graph from saved GraphDef file and returns a saver."""
    global sess

    if sess == None:
        # Creates graph from saved graph_def.pb.
        with tf.gfile.FastGFile(model_file, 'rb') as f:
            graph_def = tf.GraphDef()
            graph_def.ParseFromString(f.read())
            _ = tf.import_graph_def(graph_def, name='')

        sess = tf.Session()


def run_inference_on_image(model_file, image_file, labels_file, verbose=False):
    global sess

    answer = None
    if not tf.gfile.Exists(image_file):
        tf.logging.fatal('File does not exist %s', image_file)
        return answer

    image_data = tf.gfile.FastGFile(image_file, 'rb').read()

    # Creates graph from saved GraphDef.
    create_graph(model_file)

    # We are going to reuse the same tensorflow session
    #with tf.Session() as sess:
    softmax_tensor = sess.graph.get_tensor_by_name('final_result:0')
    predictions = sess.run(softmax_tensor,
                           {'DecodeJpeg/contents:0': image_data})
    predictions = np.squeeze(predictions)

    top_k = predictions.argsort()[-5:][::-1]  # Getting top 5 predictions
    f = open(labels_file, 'rb')
    lines = f.readlines()
    labels = [str(w).replace("\n", "") for w in lines]
    for node_id in top_k:
        human_string = labels[node_id]
        score = predictions[node_id]
        if verbose:
            print('%s (score = %.5f)' % (human_string, score))

    answer = labels[top_k[0]]
    return answer

def classify_image(image_file, verbose=False):
    """
    Quick function for quickly running the model.
    """
    model_file='resources/output_graph.pb'
    labels_file='resources/output_labels.txt'
    return  run_inference_on_image(model_file, image_file, labels_file, verbose)


def main(flags, *args, **kwargs):
    if not flags.model_file or not flags.image_file or not flags.labels_file:
        parser.print_help()
        raise ValueError("Invalid parameters...")

    return run_inference_on_image(flags.model_file, flags.image_file,
                           flags.labels_file)

# Legality - I know. I guess i wnated to talk about the postiion a little bit, and was wondering if it could be
# changed a little bit - I just got notice from the Wellesle office.
# Like Karate. I'm not sure you saw, but i'm super bad.


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--model_file',
        type=str,
        default='',
        help='Absolute path to graph model.'
    )
    parser.add_argument(
        '--image_file',
        type=str,
        default='',
        help='Absolute path to image file.'
    )
    parser.add_argument(
        '--labels_file',
        type=str,
        default='',
        help='Absolute path to labels file.'
    )
    flags, unparsed = parser.parse_known_args()
    #try_locally()
    main(flags, [sys.argv[0]] + unparsed)


