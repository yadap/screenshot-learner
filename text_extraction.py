"""
Evolve API for image classification.
"""
__author__ = "Justin Oliver and Yada Pruksachatkan"

from PIL import Image
import pytesseract
import re


def get_text(app_name, image_path):
    """
    Given an absolute path (given as a raw string),
    it returns the name and age of the person.
    """
    image = Image.open(image_path)
    text = pytesseract.image_to_string(image)

    if app_name in ('tinder', 'bumble'):
        return get_text_tinder(text)
    elif app_name == "okcupid":
        return get_text_okcupid(text)


def get_text_okcupid(text):
    """
    Given an absolute path (given as a raw string),
    it returns the name and age of the person.
    """
    import ipdb; ipdb.set_trace()
    matches = re.search("([a-z0-9_]+).*?\\n(\\d{2})\\s", text)
    if (matches is not None):
         name_age_str = matches.group()
         print name_age_str
         return name_age_str.split(', ')
    return None, None

def get_text_tinder(text):
    """
    Given an absolute path (given as a raw string),
    it returns the name and age of the person.
    """
    for i in text.split("\n"):
        matches = re.search("\w+\s\w+, [0-9 _]*[0-9]", i)
        if (matches is None):
            matches = re.search("\w+, [0-9 _]*[0-9]", i)
        if (matches is not None):
             name_age_str = matches.group()
             return name_age_str.split(', ')
    return None, None


if __name__ == '__main__':
    #path = r'/Users/yadapruksachatkun/screenshot-learner/resources/categories/tinder/2016-06-13 14.29.05.jpeg'
    path = r'/Users/justinoliver/Downloads/OKCupid 750x1330/IMG_0983.jpeg'
    print get_text('okcupid', path)

