"""
Simple file uploader to test the Evolve API.

$ python test_send.py --host "http://ec2-52-207-7-130.compute-1.amazonaws.com" --port="80"
$ python test_send.py --directory=/Users/justin/Downloads/OKCupid/ --category=okcupid
"""
__author__ = "Justin Oliver and Yada Pruksachatkan"

import argparse
from base64 import urlsafe_b64encode
import itertools
import json
import mimetools
import mimetypes
import os
from cStringIO import StringIO
import sys
import urllib2


class MultiPartForm(object):
    """Accumulate the data to be used when posting a form."""

    def __init__(self):
        self.form_fields = []
        self.files = []
        self.boundary = mimetools.choose_boundary()
        return

    def get_content_type(self):
        return 'multipart/form-data; boundary=%s' % self.boundary

    def add_field(self, name, value):
        """Add a simple field to the form data."""
        self.form_fields.append((name, value))
        return

    def add_file(self, fieldname, filename, fileHandle, mimetype=None):
        """Add a file to be uploaded."""
        body = fileHandle.read()
        if mimetype is None:
            mimetype = mimetypes.guess_type(filename)[0] or 'application/octet-stream'
        self.files.append((fieldname, filename, mimetype, body))
        return

    def __str__(self):
        """Return a string representing the form data, including attached files."""
        # Build a list of lists, each containing "lines" of the
        # request.  Each part is separated by a boundary string.
        # Once the list is built, return a string where each
        # line is separated by '\r\n'.
        parts = []
        part_boundary = '--' + self.boundary

        # Add the form fields
        parts.extend(
            [ part_boundary,
              'Content-Disposition: form-data; name="%s"' % name,
              '',
              value,
            ]
            for name, value in self.form_fields
            )

        # Add the files to upload
        parts.extend(
            [ part_boundary,
              'Content-Disposition: file; name="%s"; filename="%s"' % \
                 (field_name, filename),
              'Content-Type: %s' % content_type,
              '',
              body,
            ]
            for field_name, filename, content_type, body in self.files
            )

        # Flatten the list and add closing boundary marker,
        # then return CR+LF separated data
        flattened = list(itertools.chain(*parts))
        flattened.append('--' + self.boundary + '--')
        flattened.append('')
        return '\r\n'.join(flattened)


def classify_image(path, flags, *args, **kwargs):
    # Create the form with simple fields
    form = MultiPartForm()
    form.add_field('firstname', 'Doug')
    form.add_field('lastname', 'Hellmann')

    # Add the file
    image = StringIO(urlsafe_b64encode(open(path, "rb").read()))
    form.add_file('file', flags.path, fileHandle=image)

    # Build the request
    url = flags.host + ':' + flags.port + '/classifications/'
    request = urllib2.Request(url)
    request.add_header('User-agent', 'PyMOTW (http://www.doughellmann.com/PyMOTW/)')
    body = str(form)
    request.add_header('Content-type', form.get_content_type())
    request.add_header('Content-length', len(body))
    request.add_data(body)

    request.get_data()
    result = urllib2.urlopen(request).read()
    return json.loads(result)


def main(flags, *args, **kwargs):
    if not flags.directory:
        print classify_image(flags.path, flags, *args, **kwargs)
        return

    # Classify an entire directory
    results = []
    for subdir, dirs, files in os.walk(flags.directory):
        for image_path in files:
            path = subdir + image_path
            result = classify_image(path, flags, *args, **kwargs)
            results.append(result)
            print result

    if flags.category:
        print "Result is: "
        print all(r['tag'] == flags.category for r in results)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--path',
        type=str,
        default='resources/test_pictures/IMG_3801.jpg',
        help='Absolute path to labels file.'
    )
    parser.add_argument(
        '--host',
        type=str,
        default='http://localhost',
        help='Host url'
    )
    parser.add_argument(
        '--port',
        type=str,
        default='5000',
        help='Port of the server'
    )
    parser.add_argument(
        '--directory',
        type=str,
        default='',
        help='Classify every picture in a directory'
    )
    parser.add_argument(
        '--category',
        type=str,
        default='',
        help='Classify every picture in a directory'
    )

    flags, unparsed = parser.parse_known_args()
    main(flags, [sys.argv[0]] + unparsed)

